#Remove containers 
docker rm container2222
docker rm container3333

# create containers
docker run --name=container2222 -d -p 2222:8080 tomcat
docker run --name=container3333 -d -p 3333:8080 tomcat

#Copy war file into the 2  containers
docker cp  HelloWeb.war container2222://usr/local/tomcat/webapps/HelloWeb.war
docker cp  HelloWeb.war container3333://usr/local/tomcat/webapps/HelloWeb.war
